/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.lookup

import com.opay.core.*
import com.opay.lookup.models.BankAccountLookupRequest
import com.opay.lookup.models.BankAccountLookupResponse

/**
 * Class to perform Lookups on the OPay platform
 *
 * @param rm Request Maanger
 */
class Lookup(private val rm: RequestManager = DefaultRequestManager()) {
    companion object {
        const val PATH_BANK_ACCOUNT = "/lookup/bank-account"
    }

    /**
     * Perform a bankAccount lookup
     *
     * @param request Bank Account Lookup Request
     * @param success Success Callback
     * @param error Error Callback
     */
    fun bankAccount(request: BankAccountLookupRequest, success: SuccessCallback<BankAccountLookupResponse>, error: ErrorCallback) {
        rm.postREST(
            Lookup.PATH_BANK_ACCOUNT,
            request,
            parseResponse("lookupBankAccount", BankAccountLookupResponse::class, success),
            error
        )
    }
}