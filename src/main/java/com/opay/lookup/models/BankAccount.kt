/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.lookup.models

/**
 * Bank Account Name Lookup Request
 *
 * @param bankCode Bank Code for account number
 * @param accountNumber Account Number to lookup
 * @param countryCode Country code where the bank is located
 */
data class BankAccountLookupRequest(val bankCode: String, val accountNumber: String, val countryCode: String)

// Response is the bank account name
typealias BankAccountLookupResponse = String
