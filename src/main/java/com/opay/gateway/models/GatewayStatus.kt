/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

@file:JvmName("GatewayStatus")

package com.opay.gateway.models

const val StatusProcessing = "processing"
const val Status3DSecure = "3dsecure"
const val StatusInputOTP = "input-otp"
const val StatusInputPIN = "input-pin"
const val StatusSuccessful = "successful"
const val StatusFailed = "failed"

/**
 * Gateway Status Request
 *
 * @param token Charge Request token
 */
data class GatewayStatusRequest(val token: String)

/**
 * Gatway Status Request Response
 *
 * @param status Status of Charge Request, use this to determine the next action
 * @param token Charge Request token
 * @param reference Charge Request reference
 */
data class GatewayStatusResponse(val status: String, val token: String, val reference: String)