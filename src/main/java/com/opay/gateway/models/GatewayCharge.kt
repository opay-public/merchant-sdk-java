/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.gateway.models

typealias InstrumentType = String


internal const val Account: InstrumentType = "account"
internal const val Card: InstrumentType = "card"

/**
 * GatewayChargeRequest for creating a new Charge Request via the Gateway.create() API
 *
 * @param publicKey merchant public key
 * @param currency ISO- Currency
 * @param countryCode ISO- Country code
 * @param amount Amount to be charge from payment instrument
 * @param reference Unique reference for this charge request
 * @param instrumentType Type of Instrument for this charge (either account or card)
 * @param tokenize Whether this is a tokenize request
 * @param cardNumber Required only for card instrument types
 * @param cardDateMonth Required only for card instrument types
 * @param cardDateYear Required only for card instrument types
 * @param cardCVC Required only for card instrument types
 * @param senderAccountNumber Required only for account instrument types
 * @param bankCode Required only for account instrument types
 */
sealed class GatewayChargeRequest(
        val publicKey: String,
        val currency: String,
        val countryCode: String,
        val amount: String,
        val reference: String,
        val instrumentType: InstrumentType,
        val tokenize: Boolean = false,
        val cardNumber: String? = null,
        val cardDateMonth: String? = null,
        val cardDateYear: String? = null,
        val cardCVC: String? = null,
        val senderAccountNumber: String? = null,
        val bankCode: String? = null
)

/**
 * GatewayChargeRequest for Card Charging
 *
 * @param publicKey merchant public key
 * @param currency ISO- Currency
 * @param countryCode ISO- Country code
 * @param amount Amount to be charge from payment instrument
 * @param reference Unique reference for this charge request
 * @param tokenize Whether this is a request to tokenize instrument
 * @param cardNumber Number on Card
 * @param cardDateMonth Expiry month on card (should be 2 digits)
 * @param cardDateYear Expiry year on card (should be 2 digits)
 * @param cardCVC CVC on back of card instrument
 */
class GatewayCardChargeRequest(
        publicKey: String,
        currency: String,
        countryCode: String,
        amount: String,
        reference: String,
        tokenize: Boolean,
        cardNumber: String,
        cardDateMonth: String,
        cardDateYear: String,
        cardCVC: String)
    : GatewayChargeRequest(
        publicKey,
        currency,
        countryCode,
        amount,
        reference,
        Card,
        tokenize,
        cardNumber = cardNumber,
        cardDateMonth = cardDateMonth,
        cardDateYear = cardDateYear,
        cardCVC = cardCVC
    )

/**
 * GatewayChargeRequest for Bank Account Charging
 *
 * @param publicKey merchant public key
 * @param currency ISO- Currency
 * @param countryCode ISO- Country code
 * @param amount Amount to be charge from payment instrument
 * @param reference Unique reference for this charge request
 * @param tokenize Whether this is a tokenize request
 * @param senderAccountNumber Account number to be charged
 * @param bankCode bankCode of account to be charged
 */
class GatewayAccountChargeRequest(
        publicKey: String,
        currency: String,
        countryCode: String,
        amount: String,
        reference: String,
        tokenize: Boolean,
        senderAccountNumber: String,
        bankCode: String)
    : GatewayChargeRequest(
        publicKey,
        countryCode,
        currency,
        amount,
        reference,
        Account,
        tokenize,
        senderAccountNumber = senderAccountNumber,
        bankCode = bankCode
    )

data class GatewayChargeResponse(
        val amount: String,
        val country: String,
        val currency: String,
        val reference: String,
        val token: String
)