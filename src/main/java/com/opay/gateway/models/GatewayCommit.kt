/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.gateway.models

/**
 * Gateway Commit Request
 *
 * @param currency ISO- currency
 * @param privateKey Merchant Private Key
 * @param amount Amount in Charge request
 * @param countryCode ISO- country code
 * @param token Charge request token
 */
data class GatewayCommitRequest(
        val currency: String,
        val privateKey: String,
        val amount: String,
        val countryCode: String,
        val token: String
)

typealias GatewayCommitResponse = Boolean