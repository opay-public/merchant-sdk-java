/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.gateway.models

/**
 * 3D Secure request
 *
 * @param token Charge Request token
 */
data class Gateway3DSecureRequest(val token: String)

/**
 * Response is the 3D Secure URL
 *
 */
typealias Gateway3DSecureResponse = String