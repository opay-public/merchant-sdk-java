/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.gateway

import com.opay.core.*
import com.opay.gateway.models.*

/**
 * Gateway class for making requests against the API gateway.
 *
 *
 * @param rm Request Manager
 */
class Gateway(private val rm: RequestManager = DefaultRequestManager()) {
    companion object {
        private const val PATH_GATEWAY_CREATE = "/gateway/create"
        private const val PATH_GATEWAY_COMMIT = "/gateway/commit"
        private const val PATH_GATEWAY_STATUS = "/gateway/status"
        private const val PATH_GATEWAY_3DSECURE = "/gateway/3dsecure"
        private const val PATH_GATEWAY_INPUTOTP = "/gateway/input-otp"
        private const val PATH_GATEWAY_INPUTPIN = "/gateway/input-pin"
    }

    /**
     * Create a Payment Charge Request.
     *
     * @param request GatewayChargeRequest
     * @param success Success Callback
     * @param error Error Callback
     */
    fun create(request: GatewayChargeRequest, success: SuccessCallback<GatewayChargeResponse>, error: ErrorCallback) {
        rm.postREST(
            PATH_GATEWAY_CREATE,
            WithInput(request),
            parseResponse("gatewayCreate", GatewayChargeResponse::class, success),
            error
        )
    }

    /**
     * Perform a commit for a Charge Request
     *
     * @param request GatewayCommitRequest
     * @param success Success Callback
     * @param error Error Callback
     */
    fun commit(request: GatewayCommitRequest, success: SuccessCallback<GatewayCommitResponse>, error: ErrorCallback) {
        rm.postREST(
            PATH_GATEWAY_COMMIT,
            WithInput(request),
            parseResponse("gatewayCommit", GatewayCommitResponse::class, success),
            error
        )
    }

    /**
     * Check the current status of a charge request.
     * Check the status of the GatewayStatusResponse from the callback to determine which action to perform
     *
     * @param request GatewayStatusRequest
     * @param success Success Callback
     * @param error Error Callback
     */
    fun status(request: GatewayStatusRequest, success: SuccessCallback<GatewayStatusResponse>, error: ErrorCallback) {
        rm.postREST(
            PATH_GATEWAY_STATUS,
            request,
            parseResponse("gatewayStatus", GatewayStatusResponse::class, success),
            error
        )
    }

    /**
     * Perform a request to get the 3dSecure link for a Charge Request
     *
     * @param request Gateway3DSecureRequest
     * @param success Success Callback
     * @paran error Error Callback
     */
    fun threeDSecure(request: Gateway3DSecureRequest, success: SuccessCallback<Gateway3DSecureResponse>, error: ErrorCallback) {
        rm.postREST(
            PATH_GATEWAY_3DSECURE,
            request,
            parseResponse("gateway3DSecure", Gateway3DSecureResponse::class, success),
            error
        )
    }

    /**
     * Perform a request to submit OTP for a Charge Request
     *
     * @param request GatewayInputOTPRequest
     * @param success Success Callback
     * @param error Error Callback
     */
    fun inputOTP(request: GatewayInputOTPRequest, success: SuccessCallback<GatewayInputOTPResponse>, error: ErrorCallback) {
        rm.postREST(
            PATH_GATEWAY_INPUTOTP,
            request,
            parseResponse("gatewayInputOTP", GatewayInputOTPResponse::class, success),
            error
        )
    }

    /**
     * Perform a request to submit users PIN for a Charge Request
     *
     * @param request GatewayInputPINRequest
     * @param success Success Callback
     * @param error Error Callback
     */
    fun inputPIN(request: GatewayInputPINRequest, success: SuccessCallback<GatewayInputPINResponse>, error: ErrorCallback) {
        rm.postREST(
                PATH_GATEWAY_INPUTPIN,
                request,
                parseResponse("gatewayInputPIN", GatewayInputPINResponse::class, success),
                error
        )
    }
}