/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.core

import com.google.gson.Gson
import com.opay.OPay
import okhttp3.*
import java.io.IOException

/**
 * RequestManager handles sending request to the Opera Pay API.
 *
 * Should be used by many of the API classes.
 *
 *
 */
interface RequestManager {
    /**
     * Make request of data through the REST API
     *
     * @param path Rest API Path. This should not include the base url
     * @param data JSON serializable data object to be posted to the API
     * @param success Success Callback
     * @param error Error Callback
     */
    fun <T> postREST(path: String, data: Any, success: SuccessCallback<T>, error: ErrorCallback?)

    /**
     * Make request of data through graph api, used for APIs that don't exist via REST
     *
     * @param data GraphQL query string
     * @param success Success Callback
     * @param error Error Callback
     */
    fun <T> postGraphQL(data: String, success: SuccessCallback<T>, error: ErrorCallback?)

    /**
     * Make request of data through graph api, used for APIs that don't exist via REST
     *
     * @param data GraphQL query string
     * @param accessToken Access Token for authorization
     * @param success Success Callback
     * @param error Error Callback
     */
    fun <T> postGraphQL(data: String, accessToken: String, success: SuccessCallback<T>, error: ErrorCallback?)
}

/**
 * Default implementation of a Request Manager.
 * Uses OKHttp underneath
 *
 */
internal class DefaultRequestManager: RequestManager {
    companion object {
        const val TEST_BASE_URL = "https://bz-sandbox.opay-test.net"
        const val BASE_URL = "https://operapay.com"
    }

    private val parser = Gson()
    private val client = OkHttpClient()

    private fun baseUrl() = when (OPay.testMode) {
        true -> TEST_BASE_URL
        else ->  BASE_URL
    }

    private fun api(path: String) = baseUrl() + "/api/$path"

    override fun <T> postREST(path: String, data: Any, success: SuccessCallback<T>, error: ErrorCallback?) {
        val jsonData = parser.toJson(data)
        println(jsonData)
        val request = Request.Builder().url(api(path))
                .post(RequestBody.create(MediaType.parse("application/json"), jsonData))
                .build()

        submitRequest(request, error, success)
    }

    override fun <T> postGraphQL(data: String, success: SuccessCallback<T>, error: ErrorCallback?) {
        val jsonData = parser.toJson(mapOf("query" to data))
        println(jsonData)
        val request = Request.Builder().url(baseUrl() + "/graphql")
                .post(RequestBody.create(MediaType.parse("application/json"), jsonData))
                .header("Accept", "application/json")
                .build()
        submitRequest(request, error, success)
    }

    override fun <T> postGraphQL(data: String, accessToken: String, success: SuccessCallback<T>, error: ErrorCallback?) {
        val jsonData = parser.toJson(mapOf("query" to data))
        val request = Request.Builder().url(baseUrl() + "/graphql")
                .post(RequestBody.create(MediaType.parse("application/json"), jsonData))
                .header("Accept", "application/json")
                .header("Authorization", "token $accessToken")
                .build()
        submitRequest(request, error, success)
    }

    private fun <T> submitRequest(request: Request, error: ErrorCallback?, success: SuccessCallback<T>) {
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call?, e: IOException?) {
                error?.invoke(e)
            }

            override fun onResponse(call: Call?, response: Response) {
                val responseBody = response.body()?.string()
                when {
                    response.isSuccessful -> with(parser.fromJson(responseBody, AnyMapType) as Map<String, Any>) {
                        if (contains("errors")) {
                            error?.invoke(parseError(this))
                        } else {
                            success.invoke(parser.toJson(this["data"]) as T)
                        }
                    }
                    else -> error?.invoke(OpayError(responseBody ?: "Unknown error"))
                }
            }

        })
    }
}