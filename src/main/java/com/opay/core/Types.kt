/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.core

import com.google.gson.*
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 *
 *
 */

internal val AnyMapType = object : TypeToken<Map<String, Any>>() {}.type
internal val StringMapType = object : TypeToken<Map<String, String>>() {}.type
internal val OpayErrorsType = object : TypeToken<List<OpayError>>() {}.type

/**
 * Wrapper class for some Gateway Rest API calls
 *
 */
internal class WithInput(val input: Any)

/**
 * Service Type for a particular transaction
 *
 * @param value Service Type
 */
@JsonAdapter(ServiceType.Serializer::class)
enum class ServiceType(val value: String) {
    AIRTIME("airtime"),
    DATA("data"),
    BANK("bank"),
    ELECTRICITY("electricity"),
    TV("tv"),
    WATER("water"),
    COINS("coins");

    class Serializer: JsonSerializer<ServiceType>, JsonDeserializer<ServiceType> {
        override fun serialize(src: ServiceType, typeOfSrc: Type?, context: JsonSerializationContext): JsonElement {
            return context.serialize(src.value)
        }

        override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): ServiceType {
            return ServiceType.values().first { it.value == json.asString }
        }
    }
}

/**
 * Payment Type for a particular transaction
 *
 * @param value Payment Type
 */
@JsonAdapter(PaymentType.Serializer::class)
enum class PaymentType(val value: String) {
    COINS("coins"),
    TOKEN("token");

    class Serializer: JsonSerializer<PaymentType>, JsonDeserializer<PaymentType> {
        override fun serialize(src: PaymentType, typeOfSrc: Type?, context: JsonSerializationContext): JsonElement {
            return context.serialize(src.value)
        }

        override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): PaymentType {
            return PaymentType.values().first { it.value == json.asString }
        }
    }
}

/**
 * An AccessToken is gotten from authorization. It has an expiry timestamp and the token value
 *
 * @param expiresAt Expiry timestamp for the access token
 * @param value Access token value
 */
data class AccessToken(@SerializedName("expires_at") val expiresAt: String, val value: String)

/* Access code string, used to exchange for a Access Token through Authorizer */
typealias AccessCode = String

/**
 * Public and Private Key Pair
 *
 * @param publicKey merchant public key
 * @para privateKey merchant private key
 */
data class KeyPair(internal val publicKey: String, internal val privateKey: String)

/**
 * Represents and amount with its currency
 *
 * @param currency Currency of the amoun t
 * @param value the amount in value
 */
data class CurrencyAmount(val currency: String, val value: String)

/**
 * Cashback data type
 *
 * @param amount CurrenyAmount of cashback
 */
data class CashbackType(val amount: CurrencyAmount)
