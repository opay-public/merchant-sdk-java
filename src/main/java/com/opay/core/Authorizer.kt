/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.core

/**
 * Perform Authorization to get Access Codes and Access Token.
 * The Access Token is required to make some request through the API.
 *
 * @param rm Request Manager
 */
class Authorizer(private val rm: RequestManager = DefaultRequestManager()) {

    /**
     * Uses the merchant public key to get an accessToken from the OPay API
     *
     * @param publicKey Merchant public key
     * @param success Success Callback
     * @param error Error Callback
     */
    fun getAccessCode(publicKey: String, success: SuccessCallback<AccessCode>, error: ErrorCallback) {
        rm.postGraphQL(
            "query { accessCode(publicKey: \"$publicKey\") }",
            parseResponse("accessCode", AccessCode::class, success),
            error
        )
    }

    /**
     * Uses accessCode and privateKey to exchange for an accessToken
     *
     * @param accessCode Access Code
     * @param privateKey Merchant private key
     * @param success Success Callback
     * @param error Error Callback
     */
    fun getAccessToken(accessCode: String, privateKey: String, success: SuccessCallback<AccessToken>, error: ErrorCallback) {
        rm.postGraphQL(
            "query { accessToken(accessCode: \"$accessCode\", privateKey: \"$privateKey\") { expires_at value } }",
            parseResponse("accessToken", AccessToken::class, success),
            error
        )
    }

    /**
     * Quick function to get AccessToken
     * It fetches the Access Code and Swaps it for the AccessToken
     *
     * @param keys Merchant KeyPair used to get AccessToken
     * @param success Callback to pass successful access token
     * @param error Callback to pass error during token exchange
     */
    fun getAccessToken(keys: KeyPair, success: SuccessCallback<AccessToken>, error: ErrorCallback) {
        getAccessCode(
            keys.publicKey,
            SuccessCallback { accessCode ->
                getAccessToken(accessCode, keys.privateKey, success, error)
            },
            error
        )
    }
}