/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.core;

/**
 * Callback Interface to be passed any error occurring from asynchronous requests back to the caller.
 * This interface is used extensively through out the API
 *
 *
 */
@FunctionalInterface
public interface ErrorCallback {

    /**
     * Called with the error that occurs
     *
     * @param error Exception error
     */
    void invoke(Exception error);
}
