/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.core;

/**
 * Callback Interface to be passed response of asynchronous requests back to the caller.
 * This interface is used extensively through out the API
 *
 *
 */
@FunctionalInterface
public interface SuccessCallback<T> {

    /**
     * Method to be called with the successful result
     *
     * @param result
     */
    void invoke(T result);
}
