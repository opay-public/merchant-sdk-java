/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay.core

/**
 * Represent any errors returned by the OPay API.
 * The errors are extracted and thrown as an OpayError.
 * You should check if an exception gotten from an Error is an OpayError to handle it specifically.
 *
 *
 * @param errors List of errors returned by OPay API
 */
class OpayError(val errors: List<String>)
    : Exception(errors.joinToString()) {
    constructor(error: String): this(listOf(error))
}