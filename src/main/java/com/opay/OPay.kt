/* © 2018 OneSpot Technology Investment Ltd. All rights reserved. */

package com.opay

/**
 * Opay Configuration Object
 *
 *
 */
class OPay {
    companion object {
        @JvmStatic
        var testMode = true
    }
}